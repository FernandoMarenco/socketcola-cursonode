var socket = io();

socket.on('connect', function() {
    console.log('Conectado');

});

socket.on('disconnect', function() {
    console.log('Desconectado');
});

var searchParams = new URLSearchParams(window.location.search);

if(!searchParams.has('desk')) {
    window.location = 'index.html';
    throw new Error('Desk is necesary');
}

var desk = searchParams.get('desk');
var deskId = $('#deskId');
var ticketId = $('#ticketId');

deskId.text(desk);

$('#btnAttendNext').on('click', function() {
    socket.emit('attendTicket', { desk: desk }, function(res) {
        console.log(res);
        if(!res.number) {
            ticketId.text('No hay más tickets');
            return;
        }
        ticketId.text(res.number);
    });
});