var socket = io();

socket.on('connect', function() {
    console.log('Conectado');

});

socket.on('disconnect', function() {
    console.log('Desconectado');
});

var lblTickets = [], lblDesks = [], turns = 4;

for (let index = 1; index <= turns; index++) {
    lblTickets.push($('#lblTicket'+index));
    lblDesks.push($('#lblDesk'+index));
}

socket.on('getActual', function(data) {
    var audio = new Audio('../audio/new-ticket.mp3');
    audio.play();
    updateTurns(data.attending);
});


function updateTurns(attending) {
    attending.forEach(function(element, index) {
        lblTickets[index].text('Ticket: '+ element.number);
        lblDesks[index].text('Escritorio: '+ element.desk);
    });
}