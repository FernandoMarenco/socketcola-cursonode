var socket = io();


socket.on('connect', function() {
    console.log('Conectado');
    
});

socket.on('disconnect', function() {
    console.log('Desconectado');
});

socket.on('getActual', function(data) {
    console.log(data.last);
    label.text('Ticket: ' + data.last);
});

var label = $('#lblNuevoTicket');

$('#newTicket').on('click', function() {
    socket.emit('newTicket', null, function(next) {
        label.text('Ticket: ' + next);
    });
});