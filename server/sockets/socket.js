const { io } = require('../app');
const TicketControl = require('../classes/TicketControl');

const ticketControl = new TicketControl();

io.on('connection', (client) => {

    client.on('newTicket', (data, callback) => {
        //console.log(ticket.next());
        callback(ticketControl.next());
    });

    client.emit('getActual', {
        last: ticketControl.getLast(),
        attending: ticketControl.getAttending()
    });

    client.on('attendTicket', (data, callback) => {
        if(!data.desk) {
            return callback({
                error: 'Desk not provided'
            });
        }

        const attended = ticketControl.attendTicket(data.desk);
        callback(attended);

        //update attending
        client.broadcast.emit('getActual', {
            last: ticketControl.getLast(),
            attending: ticketControl.getAttending()
        });
    });

});