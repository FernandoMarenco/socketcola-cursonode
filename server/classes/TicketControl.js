const fs = require('fs');
const Ticket = require('./Ticket');

class TicketControl {
    
    constructor() {
        this.last = 0;
        this.today = new Date().getDate();
        this.tickets = [];
        this.attending = [];
        this.limitAttended = 4;

        const data = require('../data/data.json');

        if(data.today === this.today) {
            this.last = data.last;
            this.tickets = data.tickets;
            this.attending = data.attending;
        } else {
            this.resetDay();
        }
    }

    saveData() {
        let data = {
            last: this.last,
            today: this.today,
            tickets: this.tickets,
            attending: this.attending
        }
        const jsonData = JSON.stringify(data);

        fs.writeFileSync('./server/data/data.json', jsonData);
    }

    resetDay() {
        this.last = 0;
        this.tickets = [];
        this.attending = [];

        this.saveData();
    }

    next() {
        this.last += 1;

        const ticket = new Ticket(this.last, null);
        this.tickets.push(ticket);

        this.saveData();
        return this.last;
    }

    getLast() {
        return this.last;
    }

    getAttending() {
        return this.attending;
    }

    attendTicket(desk) {
        if(this.tickets.length === 0) {
            return 0;
        }

        const number = this.tickets[0].number;
        this.tickets.shift();

        const ticket = new Ticket(number, desk);
        this.attending.unshift(ticket);

        if(this.attending.length > this.limitAttended) {
            this.attending.pop();
        }
        console.log('Attending:', this.attending);

        this.saveData();

        return ticket;
    }
}

module.exports = TicketControl;
